export class BookingType {
    constructor(id, title, image, type) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.type = type;
    }
}
//# sourceMappingURL=bookingType.contract.js.map