import { Component } from '@angular/core';
import { BookingType } from './bookingType.contract';

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.css']
})
export class StartpageComponent {

  constructor() { }

  bookingType = [
    new BookingType(1, "Administrator portal", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "Installation"),
    new BookingType(2, "Pakke tilbud", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "DirectCategory"),
    new BookingType(3, "Book servicebesøg", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "Service"),
    new BookingType(4, "B2B", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "B2Bservice"),
    new BookingType(5, "Få et tilbud", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "Offer"),
    new BookingType(6, "Administrator portal", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "Installation"),
    new BookingType(7, "Pakke tilbud", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "DirectCategory"),
    new BookingType(8, "Book servicebesøg", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "Offer"),
    new BookingType(9, "B2B", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "Service"),
    new BookingType(10, "Få et tilbud", "https://jublo1.blob.core.windows.net/customerfiles/booking-type-repair.jpg", "B2Bservice")
    ];

}
