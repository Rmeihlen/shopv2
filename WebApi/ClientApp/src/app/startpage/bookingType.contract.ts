export class BookingType {
  constructor(
    public id: number,
    public title: string,
    public image: string,
    public type: string
  ) { }
}
