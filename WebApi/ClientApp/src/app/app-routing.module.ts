import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartpageComponent } from './startpage/startpage.component';

import { InstallationunitsComponent } from './startpage/installationunits/installationunits.component';
import { DirectcategoryComponent } from './startpage/directcategory/directcategory.component';
import { RepairsofferComponent } from './startpage/repairsoffer/repairsoffer.component';
import { ServicevisitComponent } from './startpage/servicevisit/servicevisit.component';
import { B2bserviceComponent } from './startpage/b2bservice/b2bservice.component';
import { OrderoverviewComponent } from './orderoverview/orderoverview.component';
import { ChoosetimeslotComponent } from './choosetimeslot/choosetimeslot.component';

const routes: Routes = [
  { path: '', component: StartpageComponent, pathMatch: 'full' },
  { path: 'Installation', component: InstallationunitsComponent },
  { path: 'DirectCategory', component: DirectcategoryComponent },
  { path: 'Offer', component: RepairsofferComponent },
  { path: 'Service', component: ServicevisitComponent },
  { path: 'B2Bservice', component: B2bserviceComponent },
  { path: 'OrderOverview', component: OrderoverviewComponent },
  { path: 'ChooseTimeslot', component: ChoosetimeslotComponent }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
