import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { StartpageComponent } from './startpage/startpage.component';
import { InstallationunitsComponent } from './startpage/installationunits/installationunits.component';
import { DirectcategoryComponent } from './startpage/directcategory/directcategory.component';
import { RepairsofferComponent } from './startpage/repairsoffer/repairsoffer.component';
import { ServicevisitComponent } from './startpage/servicevisit/servicevisit.component';
import { B2bserviceComponent } from './startpage/b2bservice/b2bservice.component';
import { NavigationComponent } from './navigation/navigation.component';
import { OrderoverviewComponent } from './orderoverview/orderoverview.component';
import { OrdercompleteComponent } from './ordercomplete/ordercomplete.component';
import { OrdercompletewithunitComponent } from './ordercompletewithunit/ordercompletewithunit.component';
import { ChoosetimeslotComponent } from './choosetimeslot/choosetimeslot.component';

@NgModule({
  declarations: [
    AppComponent,
    StartpageComponent,
    InstallationunitsComponent,
    DirectcategoryComponent,
    RepairsofferComponent,
    ServicevisitComponent,
    B2bserviceComponent,
    NavigationComponent,
    OrderoverviewComponent,
    OrdercompleteComponent,
    OrdercompletewithunitComponent,
    ChoosetimeslotComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
