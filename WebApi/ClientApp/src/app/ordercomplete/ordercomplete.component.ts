import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ordercomplete',
  template: `<div class="row">
  <div class="col-12 text-center">
    blah blah blah
    <br />
    <button class="btn btn-outline-secondary mr-3">Shop videre</button>
    <button class="btn btn-outline-success">Til kurv</button>
  </div>
</div>`,
  styleUrls: ['./ordercomplete.component.css']
})
export class OrdercompleteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
